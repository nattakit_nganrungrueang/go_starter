package main

import (
	"github.com/labstack/gommon/log"
)

func main() {

	log.Info(Hello("Nattakit"))
}

func Hello(name string) string {
	return "hello from starter =: " + name
}
